
function createNewUser() {

    let firstName = prompt("Ваше ім'я");
    let lastName = prompt("Ваше прізвище");
    let birthday = prompt("Ваша дата народження у форматі dd.mm.yyyy", "dd.mm.yyyy");
    
  
      const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        birthday: birthday,
  
        set firstName(value) {
          this._firstName = value;
        },
        get firstName() {
          return this._firstName;
        },
  
        set lastName(value) {
          this._lastName = value;
        },
        get lastName() {
          return this._lastName;
        },
  
        getLogin: function() {
          return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },

        getAge: function() {
          const userBirthday = new Date (this.birthday.split('.').reverse().join('-'));
          const currentDate = new Date();
          return Math.floor( (currentDate.getTime() - userBirthday.getTime()) / (1000 * 60 * 60 * 24 * 365));
        },

        getPassword: function() {
          return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        },

      }
      return newUser;
    }
    let user = createNewUser();
  
    console.log(user);
    console.log(user.getLogin());
    console.log(user.getAge());
    console.log(user.getPassword());